import { Registration, ApiCheck } from './services/index';

const apiProvider = () => ({
  registration: () => new Registration(),
  apiCheck: () => new ApiCheck(),
});

export { apiProvider };
//это посредник
