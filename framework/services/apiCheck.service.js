import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../lib/decorate';

function ApiCheck() {
  this.get = async function (value, emailAddress) {
    const r = await supertest(urls.apilayer)
      .get('/api/check')
      .query({ access_key: value, email: emailAddress });
    return r;
  };
}

decorateService(ApiCheck);

export { ApiCheck };
