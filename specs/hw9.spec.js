import { expect, test } from '@jest/globals';
import faker from 'faker';
import { apiProvider } from '../framework';

test('Проверка ответа', async () => {
  const apiChekObject = apiProvider().apiCheck();
  const r = await apiChekObject.get(
    '95479d93a01c4db2d0a8bb10f1d3f189',
    'krendel@mail.ru'
  );
  expect(r.body.email).toBe('krendel@mail.ru');
});

test('Проверка ответа, если нет access_key', async () => {
  const apiChekObject = apiProvider().apiCheck();
  const r = await apiChekObject.get('');
  expect(r.status).toBe(200);
  expect(r.body.success).toBe(false);
});

test.each`
  email
  ${'qwerty'}
  ${'qwerty@gmail.com'}
  ${'qwerty@gmail.ru'}
  ${'qwerty@gmailcom'}
  ${'@gmail.com'}
  ${faker.internet.email()}
  ${'\tqwerty@gmail.com\t'}
  ${'qwerty\t@gmail.com'}
  ${' qwerty@gmail.com '}
  ${'QWERTY@GMAIL.COM'}
`('$email', async ({ email }) => {
  const apiChekObject = apiProvider().apiCheck();
  const r = await apiChekObject.get('95479d93a01c4db2d0a8bb10f1d3f189', email);
  expect(r.status).toBe(200);
  expect(r.body.success).toBe(undefined);
});

test.each`
  email
  ${''}
  ${null}
`('$email', async ({ email }) => {
  const apiChekObject = apiProvider().apiCheck();
  const r = await apiChekObject.get('95479d93a01c4db2d0a8bb10f1d3f189', email);
  expect(r.status).toBe(200);
  expect(r.body.success).toBe(false);
});
